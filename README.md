# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : "Let's Chat"
* Key functions 
    1. 1-to-1 chat
    2. Global chat
    3. Load chat history 
    4. Chat with new user

* Other functions 
    1. Search username
    2. Add friends

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-501ee.firebaseapp.com

# Components Description : 
1. 1-to-1 chat:
    >藉由添加其他使用者好友，可以與好友1對1單獨聊天
2. Global chat
    >每一個使用者都擁有的世界頻道，可以在此與全部使用者一同聊天<br>
    >就算不是好友，也可以與對方聊天(聊天內容會被其他使用者看到)
3. Load chat history
    >與好友的每一次聊天都是一段美好的記憶<br>
    >然而人的記憶有限，怎能通通記在腦海裡。<br>
    >怕忘記嗎?不要緊的，"Let's Chat"通通幫你記得<br>
    >與朋友的每一句對話都會記錄起來，隨時等候你來回憶
4. Chat with new user
    >新用戶也可以與舊用戶享受同等的款待，可以被加為好友，也可以看到世界頻道以往的對話紀錄

# Other Functions Description(1~10%) : 
1. Search username
    >為了怕用戶被陌生人打攪，"Let's Chat"不會公布所有使用者，但可以利用搜尋功能，找尋其他使用者的username，並加為好友。<br>
    >若搜尋未存在的使用者名稱，網頁會告知不存在此使用者<br>
    --------------------------------
    >我把使用者的username紀錄在database裡，並且利用<code>firebase.database().ref('user_name')</code>搭配<code>orderByKey()</code>和<code>equalTo()</code>達成搜尋功能。並根據回傳的資料判定是否有此username
2. Add Friends
    >承Search功能，當使用者成功搜尋到其他使用者，可以加為好友，即可與之聊天，當下次登入時，好友名單並不會重置。<br>
    >當然，當被別人加為好友時，亦會顯示出來<br>
    >"Let's Chat"鼓勵使用者多與人交流，因此並無提供加自己為好友的功能。若真的想與自己聊天，"Let's Chat"建議可以辦兩個帳號<br>
    -------------------------
    >在我的database之中，有建立每個使用者的好友名單，當加好友時，變會利用<code>firebase.database().ref('user_name/<b>使用者名稱</b>')</code>與<code>push()</code>搭配，將好友存入每個使用者旗下的好友名單(好友那邊亦會執行此步驟)

## Security Report
1. 在<b>database.rules.json</b>檔中，把<b>write</b>改成<code>"auth!=null"</code>，至於為甚麼不改<b>read</b>，是因為我在sign-up的時候需要讀取database中的資料，以檢查username是否重複
2. 使用<code>firebase.auth().onAuthStateChanged</code>判斷是否有用戶登入，若無，則會隱藏好友欄的所有按鈕與資訊，且對話框也不會有任何資訊，輸入框雖可打字，卻無任何效用
3. 在我建立的database中，需要通過多層路徑才可以讀取到資料，而路徑又與使用者和好友有關，使安全性更加提升
4. 使用<code>change()</code>轉換每一次輸入框的value中的特殊字元，以防止有人在輸入框內輸入HTML的程式碼，攻擊網頁。
如以下<br>
<code>\<button onclick="firebase.auth().currentUser.updatePassword('123456').then(()=>{
  firebase.auth().signOut().then(()=>{})
});">QAQ\</button></code><br>
會創造一個按鈕，會使點擊的使用者登出，並更改其密碼。