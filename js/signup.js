function initApp(){
  var txtEmail = document.getElementById("inputSignEmail");
  var txtPassword = document.getElementById("inputSignPassword");
  var txtUserName = document.getElementById("inputSignUserName");
  var buttonsignUp = document.getElementById("signAuthUp");
  var backindex = document.getElementById("indexUp");
  buttonsignUp.addEventListener("click",()=>{
    firebase.database().ref('user_name').orderByKey().equalTo(txtUserName.value).on('value',(data)=>{
      
      if(data.val()){
        if(data.child("qwertyuiopQAQ") == ""){

        }
        else{
          create_alert("error","The User Name had been used!");
        }
      }
      else{
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then((success)=>{
          firebase.auth().signInWithEmailAndPassword(txtEmail.value,txtPassword.value).then((su)=>{
            let user = firebase.auth().currentUser;
            user.updateProfile({
              displayName:txtUserName.value
            }).then((success)=>{
              console.log(txtUserName.value);
              var name = txtUserName.value;
              firebase.database().ref("user_name").child(txtUserName.value).update({qwertyuiopQAQ:""}).then((res)=>{
                window.location.href = "index.html";
              }) 
            }).catch((e)=>{
              console.log(e.message);
            })
          })
        })
        .catch((error)=>{
          create_alert("error",error.message);
          txtEmail.value="";
          txtPassword.value="";
          txtUserName.value="";
        })
      }
    })
  })

  backindex.addEventListener("click",()=>{
    window.location.href="index.html";
  })

}

function create_alert(type, message) {
  var alertarea = document.getElementById('alert_custom_signUp');
  if (type == "success") {
      str_html = "<div class='alert alert-success alert-dismissible fade show alert_' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  } else if (type == "error") {
      str_html = "<div class='alert alert-danger alert-dismissible fade show alert_' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  }
}

window.onload = function () {
  initApp();
};