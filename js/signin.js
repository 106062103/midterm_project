function initApp(){
  var txtEmail = document.getElementById("inputEmail");
  var txtPassword = document.getElementById("inputPassword");
  var buttonsignIn = document.getElementById("signIn");
  var buttonsignUp = document.getElementById("signUp");
  var buttonGoogle = document.getElementById("btnGoogle");
  var back_index = document.getElementById("indexIn");
  var buttonForget = document.getElementById("btnForget");
  buttonsignIn.addEventListener("click",()=>{
    console.log([txtEmail.value,txtPassword.value]);

    firebase.auth().signInWithEmailAndPassword(txtEmail.value,txtPassword.value).then(()=>{
      window.location.href="index.html";
      
    })
    .catch((error)=>{
      create_alert('error',error.message);
      txtEmail.value="";
      txtPassword.value="";
    })

    
  })

  buttonForget.addEventListener("click",()=>{
    create_alert("Sorry","We don't provide the service")
  })

  buttonGoogle.addEventListener("click",()=>{
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then((result)=>{
        var token = result.credential.accessToken;
        var user = result.user;
        window.location.href =  'index.html';
    })
    .catch((error)=>{
        var errorcode = error.message;
        create_alert('error',errorcode);
    })
  })

  buttonsignUp.addEventListener("click",()=>{
    window.location.href="signup.html";
  })

  back_index.addEventListener("click",()=>{
    window.location.href="index.html";
  })


}

function create_alert(type, message) {
  var alertarea = document.getElementById('alert-custom');
  if (type == "success") {
      str_html = "<div class='alert alert-success alert-dismissible fade show alert_' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  } else if (type == "error") {
      str_html = "<div class='alert alert-danger alert-dismissible fade show alert_' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  } else if(type == "Sorry"){
      str_html = "<div class='alert alert-danger alert-dismissible fade show alert_' role='alert'><strong>Sorry! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  }
}

window.onload = function () {
  initApp();
};