var userName="";
var chattingFriend = document.getElementById("currentFriend");
var talking = document.getElementById("talking");
var nowFriend="";
var back_index=document.getElementById("index_");
var messageListen;
function init(){
  var userFriend = document.getElementById("friend");
  firebase.auth().onAuthStateChanged((user)=>{
    if(user){
      userName = user.displayName;
      document.getElementById("loginBar").innerHTML =`<div class='user-name'>${userName}</div>` +"<div class='logout-pic'><img src='logout.png' class='img-fluid' style='max-width:100%;height:auto' id=logout-btn></div>";
      console.log(user);

      var logout = document.getElementById("logout-btn");
      logout.addEventListener('click',()=>{
        firebase.auth().signOut().then(()=>{

        }).catch((error)=>{
          var errorcode = error.message;
          console.log(errorcode);
        })
      })
      
    }
    else{
      document.getElementById("loginBar").innerHTML = "<a href='sign.html'>Login</a>";
      userFriend.innerHTML ="";
      chattingFriend.innerHTML ="Welcome To ChatRoom~";
      talking.innerHTML ="";
    }
  })
  

  var text_content = document.getElementById("text_content");
  var post_button = document.getElementById("send_text");

  back_index.addEventListener("click",()=>{
    window.location.href="index.html"
  })

  post_button.addEventListener("click",()=>{
    var current_user_for_post = firebase.auth().currentUser;
    console.log(nowFriend); 
    if(text_content.value!="" && current_user_for_post && nowFriend=="global"){
      console.log("global");
      var text_global = firebase.database().ref('global');
      var time_global = getDate();
      text_global.push().set({
        data:change(text_content.value),
        from:userName,
        time:time_global
      })
      text_content.value="";
    }
    else if(text_content.value!="" && current_user_for_post && nowFriend!=""){
      console.log("OUT");
      var text_ref = firebase.database().ref('user_name/'+userName+"/"+nowFriend);
      var text_ref_friend = firebase.database().ref('user_name/'+nowFriend+"/"+userName);
      var time_ = getDate();
      text_ref.push().set({
        data:change(text_content.value),
        from:0,
        time:time_,
      })
      text_ref_friend.push().set({
        data:change(text_content.value),
        from:1,
        time:time_
      })
      text_content.value="";
    }
    else{
      text_content.value="";
    }
  })

  var com_list = firebase.database().ref('user_name');
  var total_friend=[];
//要改成promise

  window.setTimeout(()=>{
    try{
      userName = firebase.auth().currentUser.displayName;
      com_list.child(userName).once('value')
      .then((snapshot)=>{
        snapshot.forEach((ssnapshot) => {
          console.log(ssnapshot.key);
          if(ssnapshot.key!="qwertyuiopQAQ"){
            userFriend.innerHTML = "<button style='margin-top:20%;' class='btn btn-block btn-primary' onclick=\"changeChattingPerson('"+String(ssnapshot.key)+"')\">"+ssnapshot.key+"</button>" + userFriend.innerHTML ;        
          }
        });
        com_list.child(userName).on('value',(snapshot)=>{
          total_friend.length=0;
          snapshot.forEach((ssnapshot)=>{
            if(ssnapshot.key!="qwertyuiopQAQ"){
              total_friend.push("<button style='margin-top:20%' class='btn btn-block btn-primary' onclick=\"changeChattingPerson('"+String(ssnapshot.key)+"')\">"+ssnapshot.key+"</button>")
            }
          })
          userFriend.innerHTML = "<button style='margin-top:20%' class='btn btn-block btn-info' onclick=chattingGlobal()>聊天大廳</button>"+total_friend.join("") + "<button style='margin-top:20%' class='btn btn-block btn-outline-primary' onclick=plusNewFriend()>+</button><input type='text' id='newFriend' placeholder='Search a user~' style='width:100%;margin-top: 5%'>";
        })

        com_list.child(userName).on('child_changed',(snapshot)=>{
          console.log(snapshot.key);
          //console.log( snapshot.val()[Object.keys(snapshot.val())[Object.keys(snapshot.val()).length-1]] );
          var obj = snapshot.val()[Object.keys(snapshot.val())[Object.keys(snapshot.val()).length-1]];
          console.log(obj.data);
          if(obj.from !=0){
            if(Notification.permission == "granted"){
              popNotice(snapshot.key,obj);
            }
            else if(Notification.permission =="default"){
              Notification.requestPermission((permission)=> {
                
              });
            }
          }
        })

      })
    }
    catch(e){
      console.log(e.message);
    }
  },1000)

  /*if(window.Notification){
    firebase.database().ref('user/'+)
  }
  else{
    alert('此瀏覽器不支援Notification');
  }*/
}

function getDate(){
  var date = new Date();
  var hour = "";
  var minute = "";
  if(date.getHours()<10){
    hour = `0${date.getHours()}`;
  }
  else{
    hour = `${date.getHours()}`;
  }
  if(date.getMinutes()<10){
    minute = `0${date.getMinutes()}`;
  }
  else{
    minute = `${date.getMinutes()}`;
  }
  var time_date = `${date.getFullYear()}/${date.getMonth()+1}/${date.getDate()} ${hour}:${minute}`;
  return time_date;
}

function change(ch){
  if (ch===null) return '';
  ch = ch.replace(/&/g,"&amp;");
  ch = ch.replace(/\"/g,"&quot;");
  ch = ch.replace(/\'/g,"&#039;");
  ch = ch.replace(/</g,"&lt;");
  ch = ch.replace(/>/g,"&gt;");
  return ch;
}

function popNotice(from,obj){
  if (Notification.permission == "granted") {
    var notification = new Notification("From : " + from, {
        body:obj.data 
    });
    
    notification.onclick = function() {
      window.location.href="index.html";    
    };
}
}

window.onload = ()=>{
  init();
}

function plusNewFriend(){
  var newFriend = document.getElementById("newFriend");
  if(newFriend.value == userName){
    create_alert("error","(ಥ﹏ಥ) So Lonely (ಥ﹏ಥ)");
    newFriend.value="";
  }else{
    firebase.database().ref('user_name').orderByKey().equalTo(newFriend.value).on('value',(data)=>{
      if(data.val()){
        console.log(data.val());
        var friend_list = firebase.database().ref('user_name/'+userName+'/'+newFriend.value);
        friend_list.push({
          data:"",
          from:0,
          time:0
        })
        //firebase.database().ref('user_name/'+userName).set(newFriend.value);
      }
      else{
        create_alert("error","Cannot find the user \""+newFriend.value+'"');
        newFriend.value="";
      }
    })
  }
  
}

function create_alert(type, message) {
  var alertarea = document.getElementById('alert_custom_index');
  if (type == "success") {
      str_html = "<div class='alert alert-success alert-dismissible fade show alert_' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  } else if (type == "error") {
      str_html = "<div class='alert alert-danger alert-dismissible fade show alert_' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  }
}

function chattingGlobal(){
  nowFriend="global";
  chattingFriend.innerHTML ="<p>聊天大廳</p>";
  var total_chat=[];
  var chatHistory = firebase.database().ref('global');
  chatHistory.once('value')
    .then((snapshot)=>{
      snapshot.forEach((ssnapshot)=>{
        if(ssnapshot.child("data").val()!=""){
          if(ssnapshot.child("from").val() == userName){
            total_chat.push(`
            <div class='from_0'>
              <small><b>`+ssnapshot.child("from").val()+`</b></small><br>
              <font size="3">`+ssnapshot.child("data").val()+`</font><br>
              <font size="2">`+ssnapshot.child("time").val()+`</font> 
            </div>`
            )
          }
          else{
            total_chat.push(`
            <div class='from_1'>
              <small><b>`+ssnapshot.child("from").val()+`</b></small><br>
              <font size="3">`+ssnapshot.child("data").val()+`</font><br>
              <font size="2">`+ssnapshot.child("time").val()+`</font>
            </div>`
          )
          }
        }
      })
      talking.innerHTML = total_chat.join("");
      
      chatHistory.on('value',(snapshot)=>{
        total_chat.length = 0;
        snapshot.forEach((ssnapshot)=>{

          if(ssnapshot.child("data").val()!=""){
            if(ssnapshot.child("from").val() == userName){
              total_chat.push(`
              <div class='from_0'>
                <small><b>`+ssnapshot.child("from").val()+`</b></small><br>
                <font size="3">`+ssnapshot.child("data").val()+`</font><br>
                <font size="2">`+ssnapshot.child("time").val()+`</font> 
              </div>`
            )
            }
            else{
              total_chat.push(`
              <div class='from_1'>
                <small><b>`+ssnapshot.child("from").val()+`</b></small><br>
                <font size="3">`+ssnapshot.child("data").val()+`</font><br>
                <font size="2">`+ssnapshot.child("time").val()+`</font>
              </div>`
            )
            }
          }
        })
        talking.innerHTML = total_chat.join("");
      })

    })
}

function changeChattingPerson(string){
  nowFriend=string;
  chattingFriend.innerHTML = "<p>"+string+"</p>";
  var total_chat = [];
  var chatHistory = firebase.database().ref('user_name/'+ userName+'/'+string);
  chatHistory.once('value')
    .then((snapshot)=>{
      snapshot.forEach((ssnapshot)=>{
        //total_chat.push("<p>"+ssnapshot.child("data").val() +"</p>");
        if(ssnapshot.child("data").val()!=""){
          if(ssnapshot.child("from").val() == 0){
            total_chat.push(`
            <div class='from_0'>
              <small><b>`+userName+`</b></small><br>
              <font size="3">`+ssnapshot.child("data").val()+`</font><br>
              <font size="2">`+ssnapshot.child("time").val()+`</font> 
            </div>`
          )
          }
          else if(ssnapshot.child("from").val() == 1){
            total_chat.push(`
            <div class='from_1'>
              <small><b>`+string+`</b></small><br>
              <font size="3">`+ssnapshot.child("data").val()+`</font><br>
              <font size="2">`+ssnapshot.child("time").val()+`</font>
            </div>`
          )
          }
        }

      })
      talking.innerHTML = total_chat.join("");

      chatHistory.on('value',(snapshot)=>{
        total_chat.length = 0;
        snapshot.forEach((ssnapshot)=>{

          if(ssnapshot.child("data").val()!=""){
            if(ssnapshot.child("from").val() == 0){
              total_chat.push(`
              <div class='from_0'>
                <small><b>`+userName+`</b></small><br>
                <font size="3">`+ssnapshot.child("data").val()+`</font><br>
                <font size="2">`+ssnapshot.child("time").val()+`</font> 
              </div>`
            )
            }
            else if(ssnapshot.child("from").val() == 1){
              total_chat.push(`
              <div class='from_1'>
                <small><b>`+string+`</b></small><br>
                <font size="3">`+ssnapshot.child("data").val()+`</font><br>
                <font size="2">`+ssnapshot.child("time").val()+`</font>
              </div>`
            )
            }
          }


        })
        talking.innerHTML = total_chat.join("");
      })

    })
}